**Table of Contents**:

- [Install Go](#install-go)
- [Get the project](#get-the-project)

## Install Go

  Follow the instructions at https://golang.org/doc/install

## Get the project

```sh
$ go get github.com/chrislusf/gleam/flow
$ go get github.com/chrislusf/gleam/distributed/gleam
```
