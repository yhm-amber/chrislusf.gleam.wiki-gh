### My code hangs, why?

There could be many different reasons. The common ones:
1. Avoid printing to stdout. Write to stderr via `println()`, or `fmt.Fprintf(os.stderr, "...", ...)`. This is because gio.Emit() actually uses stdout to output data to stdin of the next step.
2. Use `flag` package, instead of using `os.Args` directly. Because Gleam invokes each executor via `flag` package. Multiple ways to parse arguments would cause confusion.