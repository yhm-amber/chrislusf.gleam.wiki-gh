Suppose you need to read a csv file.

## HDFS ##
```f.Read(file.Csv("hdfs://namenode:port/path/to/a.csv"))```

## Local ##
```f.Read(file.Csv("/path/to/a.csv"))```

## S3 ##
```f.Read(file.Csv("s3://bucket_name/path/to/a.csv"))```
