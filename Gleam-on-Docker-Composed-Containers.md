To run Gleam on docker containers is easy.

# Create the docker containers

## Create the docker image
```sh
FROM alpine:latest
MAINTAINER Chris Lu <chris.lu@gmail.com>

EXPOSE 45326

VOLUME /data

COPY gleam /usr/bin/
COPY entrypoint.sh /entrypoint.sh

RUN chmod +x /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
```
Since we build the image from alpine, the "chrislusf/gleam" image size is only 36.9 MB.

## Compose the containers
After creating docker file for Gleam, we can compose a docker cluster. This docker-compose.yaml file has setup one gleam master and 3 gleam agents.

```yaml
version: '2'

services:
  master:
    image: chrislusf/gleam
    command: "master"
  agent_1:
    image: chrislusf/gleam
    command: 'agent --memory=3000 --master=master:45326'
    depends_on: 
     - master
  agent_2:
    image: chrislusf/gleam
    command: 'agent --memory=3000 --master=master:45326'
    depends_on: 
     - master
  agent_3:
    image: chrislusf/gleam
    command: 'agent --memory=3000 --master=master:45326'
    depends_on: 
     - master
```
We are not exposing any port externally. Since the computation will all happen within the cluster.

Each agent is allocated 3000 MB memory, and connected to master via the host name "master".

Starting the containers is fast. It took about 2~8 seconds from "docker-compose up" to see the agents all connected to the master. The time variation seems caused by docker network itself.

```sh
chris$ date ; docker-compose up
Tue Jan 31 22:18:23 PST 2017
Starting docker_master_1
Starting docker_agent_1_1
Starting docker_agent_3_1
Starting docker_agent_2_1
Attaching to docker_master_1, docker_agent_1_1, docker_agent_2_1, docker_agent_3_1
agent_2_1  | starting in /data
master_1   | master listening on :45326
master_1   | 2017/02/01 06:18:25 added agent: data_center:"defaultDataCenter" rack:"defaultRack" server:"172.18.0.3" port:45327
agent_2_1  | AgentServer starts on 172.18.0.4:45327
agent_2_1  | 2017/02/01 06:18:25 Heartbeat to master:45326
agent_1_1  | starting in /data
agent_3_1  | starting in /data
master_1   | 2017/02/01 06:18:25 added agent: data_center:"defaultDataCenter" rack:"defaultRack" server:"172.18.0.4" port:45327
agent_3_1  | AgentServer starts on 172.18.0.5:45327
agent_3_1  | 2017/02/01 06:18:25 Heartbeat to master:45326
agent_1_1  | AgentServer starts on 172.18.0.3:45327
master_1   | 2017/02/01 06:18:25 added agent: data_center:"defaultDataCenter" rack:"defaultRack" server:"172.18.0.5" port:45327
agent_1_1  | 2017/02/01 06:18:25 Heartbeat to master:45326
```

# Run driver program

To run in the docker cluster, we need to connect to Gleam master via hostname "master".
```go
f.Run(distributed.Option().SetMaster("master:45326"))
```
The source code is https://github.com/chrislusf/gleam/blob/master/examples/word_count_in_go/word_count_in_go.go

Here are the commands to run in docker, taken from the related section of "examples/word_count_in_go/Makefile"
```sh
	GOOS=linux go build
	docker cp ./word_count_in_go docker_master_1:.
	# somehow the equal sign is required in "-onDocker=true"
	docker exec docker_master_1 ./word_count_in_go -onDocker=true
```
Because the binary should run on docker's linux instance, it has to be compiled with GOOS=linux.

Then we copy the file into one docker instance. Here I choose "docker_master_1", but it can be any one of the containers.

With the single binary file copied, we can start to execute it in distributed mode.